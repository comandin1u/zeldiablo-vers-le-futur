package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import MonJeu.Aventurier;
import MonJeu.Labyrinthe;
import MonJeu.Potion;

public class TestPotion {

	@Test
	public void testConstructeurPotion() {
		
			//potion
			Potion p = new Potion(0, 0, new Labyrinthe());
				
			//assertion
			assertEquals("X devrait etre a 0",0 , p.getX());
			assertEquals("Y devrait etre a 0",0 , p.getY());
	}
	
	@Test
	public void testConstructeurPotion2() {
		
		//potion
		Potion po = new Potion(10, 16, new Labyrinthe());
			
		//assertion
		assertEquals("X devrait etre a 10",10 , po.getX());
		assertEquals("Y devrait etre a 16",16 , po.getY());
}
	
	
}
