package Test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import MonJeu.Labyrinthe;
import MonJeu.Monstre;

public class TestMonstre {
	
	@Test
	public void testConstructeurMonstre() {
		//creation d un monstre
		Monstre m = new Monstre(0,0, new Labyrinthe());
		
		//assertion
		assertEquals("X devrait etre a 0",0 , m.getX());
		assertEquals("Y devrait etre a 0",0 , m.getY());
	}
	
	@Test
	public void testDeplacementVersMur() {
		//creation d un monstre
		Monstre m = new Monstre(2, 1, new Labyrinthe());
		
		//appel methode
		m.deplacement(2);
		
		//assertion
		assertEquals("X devrait etre a 2", 2 , m.getX());
		assertEquals("Y devrait etre a 1", 1 , m.getY());
	}
	
	@Test
	public void testDeplacement() {
		//creation d un monstre
		Monstre m = new Monstre(2, 1, new Labyrinthe());
		
		//appel methode
		m.deplacement(3);
		
		//assertion
		assertEquals("X devrait etre a 2", 2 , m.getX());
		assertEquals("Y devrait etre a 2", 2 , m.getY());
	}
	
	@Test
	public void testDeplacementsMultiple() {
		//creation d un monstre
		Monstre m = new Monstre(2, 1, new Labyrinthe());
		
		//appel methode
		m.deplacement(3);
		m.deplacement(1);
		
		//assertion
		assertEquals("X devrait etre a 3", 3 , m.getX());
		assertEquals("Y devrait etre a 3", 2 , m.getY());
	}
}
