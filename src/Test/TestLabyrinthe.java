package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import MonJeu.Aventurier;
import MonJeu.Jeu;
import MonJeu.Labyrinthe;
import MonJeu.Personnage;

public class TestLabyrinthe {

	@Test
	public void testConstructeurLabyrinthe() {
		//labyrinthe
		Labyrinthe l = new Labyrinthe();
		
		//assertion
		assertEquals("La longueur devrait etre de 16 par default", 16, l.getLabyrinthe().length);
		
	}
	
	@Test
	public void testSetLabyrinthe(){
		//labyrinthe
		Labyrinthe l = new Labyrinthe();
		l.setLabyrinthe(0, 0, 2);
		
		//assertion
		//TODO
		//assertEquals("La valeur de la case devrait etre de 2", 2, l.getLabyrinthe()[0][0]);
	} 
	
	@Test
	public void testEstUnMur(){
		//labyrinthe
		Labyrinthe l = new Labyrinthe();
		
		//assertion
		//TODO
		//assertEquals("Cette case devrait etre un mur", true, l.estUnMur(13, 13));
		
	}
	
	@Test
	public void testAjoutPersonnage(){
		//labyrinthe
		Labyrinthe l = new Labyrinthe();
		Aventurier a = new Aventurier(1, 1, l);
		//assertion
		//TODO
		//assertEquals("La valeur de la case devrait etre de 2", 2, l.getLabyrinthe()[1][1]);
		
	}
		
}
