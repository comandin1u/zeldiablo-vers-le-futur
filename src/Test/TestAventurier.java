package Test;

import static org.junit.Assert.*;
import moteurJeu.Commande;
import moteurJeu.MoteurGraphique;

import org.junit.Test;

import MonJeu.Aventurier;
import MonJeu.DessinMonJeu;
import MonJeu.Jeu;
import MonJeu.Labyrinthe;

public class TestAventurier {

	@Test
	public void testConstructeurAventurier() {
		
		//aventurier
		Aventurier a = new Aventurier(0, 0, new Labyrinthe());
		
		//assertion
		assertEquals("Les pvs devraient etre a 100",100 , a.getPdv());
		assertEquals("X devrait etre a 0",0 , a.getX());
		assertEquals("Y devrait etre a 0",0 , a.getY());
	}
	
	@Test
	public void testConstructeurAventurier2() {
		
		//aventurier
		Aventurier av = new Aventurier(2, 4, new Labyrinthe());
		
		//assertion
		assertEquals("Les pvs devraient etre a 100",100 , av.getPdv());
		assertEquals("X devrait etre a 2",2 , av.getX());
		assertEquals("Y devrait etre a 4",4 , av.getY());
	}
	
	
	@Test
	public void testSeDeplacer(){
		
		 //jeu avec aventurier
//		 Jeu jeu = new Jeu();
//		 int xd = jeu.getAventurier().getX();
//		 jeu.getAventurier().seDeplacer(new Commande(new Commande().droite));
//
//		// assertion
//		 assertEquals("Le personnage devrait s'etre deplac�", xd,
//		 jeu.getAventurier().getX());
		
	}
	
	public void testSetPosition(){
		
		//aventurier
		Aventurier a = new Aventurier(0, 0, new Labyrinthe());
		a.setPosition(2, 2);
		
		//assertion
		assertEquals("X devrait etre a 2",2 , a.getX());
		assertEquals("Y devrait etre a 2",2 , a.getY());
	}

}
