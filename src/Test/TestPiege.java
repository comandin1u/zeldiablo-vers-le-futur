package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import MonJeu.Labyrinthe;
import MonJeu.Piege;

public class TestPiege {

	@Test
	public void testConstructeurPiege() {
		
		//piege
		Piege p = new Piege(0,0, new Labyrinthe());
		
		//assertion
		assertEquals("X devrait etre a 0",0,p.getX());
		assertEquals("Y devrait etre a 0",0,p.getY());
		
	}
	
	@Test
	public void testConstructeurPiege2() {
		
		//piege
		Piege p = new Piege(1,3, new Labyrinthe());
		
		//assertion
		assertEquals("X devrait etre a 1",1,p.getX());
		assertEquals("Y devrait etre a 3",3,p.getY());
		
	}
	

}
