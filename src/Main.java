import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Editeur.Fenetre;
import MonJeu.DessinMonJeu;
import MonJeu.Jeu;
import moteurJeu.MoteurGraphique;
import sun.audio.AudioData;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import sun.audio.ContinuousAudioDataStream;

public class Main {

	/**
	 * methode principale permettant de creer un jeu, un moteur graphique et DessinMonJeu
	 * la methode appelle ensuite lancerJeu() sur le MoteurGraphique cree
	 * @param args
	 * @throws InterruptedException
	 * @throws IOException 
	 */
	
	
	public static void main(String[] args) throws InterruptedException, IOException {
		JFrame frame = new JFrame("2016 - Projet COO (Wilmouth, Comandini, Lagab, Pecron)");

		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(400, 400));        
		panel.setLayout(new BorderLayout());
		
		Jeu jeu = new Jeu();
		DessinMonJeu dessinJeu = new DessinMonJeu(jeu);
		MoteurGraphique moteur = new MoteurGraphique(jeu, dessinJeu);
		JButton jouer = new JButton("Jouer");
		jouer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new Thread() {
					@Override
					public void run() {
						try {
							moteur.lancerJeu(400,  400);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}.start();
			}
		});
		panel.add(jouer, BorderLayout.NORTH);
		
		JButton editeur = new JButton("Editeur");
		editeur.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new Fenetre("Editeur", 401, 600);
			}
		});
		panel.add(editeur, BorderLayout.SOUTH);
		
		JLabel fond = new JLabel(new ImageIcon(ImageIO.read(new File("sprites/menu.png"))));
		
		panel.add(fond, BorderLayout.CENTER);
		
		frame.setContentPane(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setPreferredSize(new Dimension(400, 400));
		frame.pack();
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
	
		music();
	}
	
	 public static void music(){     
		 
	     AudioStream BGM;
	     AudioData MD;
	     ContinuousAudioDataStream loop;

	     
	     try{
	        InputStream test = new FileInputStream("musiques/ryuthememetal.wav");
	        BGM = new AudioStream(test);
	        AudioPlayer.player.start(BGM);
	        MD = BGM.getData();
            loop = new ContinuousAudioDataStream(MD);
            AudioPlayer.player.start(loop);
	     }
	     
	     catch(FileNotFoundException e){
	        System.out.print(e.toString());
	     }
	     catch(IOException error){
	        System.out.print(error.toString());
	     }
	 }
}
