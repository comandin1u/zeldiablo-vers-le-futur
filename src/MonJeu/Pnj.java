package MonJeu;

import moteurJeu.Commande;

public class Pnj extends Personnage {
	
	private String dialogue;
	private String[] ts;
	private int nbDialogue = 0;
	private int idPNJ;
	public boolean showDialogue;
	
	public Pnj(int dx, int dy, Labyrinthe l, int idPNJ){
		super(dx, dy, l);
		this.dialogue = null;
		this.ts = new String[10];
		this.idPNJ = idPNJ;
//		this.ts[0] = "Hello !";
//		this.ts[1] = "Fdp";
		this.showDialogue = false;
	}
	
	@Override
	public void seDeplacer(Commande c) {
		// TODO Auto-generated method stub
		
	}

	public void setDialogue(String n){
		this.dialogue = n;
	}
	
	public String getDialogue(){
		return this.dialogue;
	}
	
	public void addDialogue(String s){
		ts[nbDialogue] = s;
		nbDialogue++;
	}
	
	public String[] getTS(){
		return this.ts;
	}
	
	public int getID(){
		return idPNJ;
	}
	
	public void setIDPNJ(int n){
		idPNJ = n;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "7";
	}
	
}
