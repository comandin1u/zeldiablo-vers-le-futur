package MonJeu;

import java.util.Random;

import moteurJeu.Commande;

/**
 * @author Steven
 *
 */
public class Monstre extends Personnage {

	private int direction;

	/**
	 * methode permettant de construire un monstre
	 * @param dx
	 * @param dy
	 * @param l
	 */
	public Monstre(int dx, int dy, Labyrinthe l){
		super(dx, dy, l);
		this.direction = 1;
	}

	/**
	 * seDeplacer, permet les deplacements des monstres
	 * @param commande a null car on interagit pas avec le deplacement des monstre
	 */
	@Override
	public void seDeplacer(Commande c) { 
		int direction = new Random().nextInt((4 - 0) + 1);
		this.deplacement(direction);
	}

	/**
	 * Deplacement du monstre
	 * @param direction
	 */
	public void deplacement(int direction) {
		int x = this.getX();
		int y = this.getY();
		switch (direction) {
		case 0:
			x--;
			this.direction = 0;
			break;

		case 1:
			x++;
			this.direction = 1;
			break;

		case 2:
			y--;
			this.direction = 2;
			break;

		case 3:
			y++;
			this.direction = 3;
			break;
		}
		if (x == this.getLabyrinthe().getSortie()[0] && y == this.getLabyrinthe().getSortie()[1]) return;

		if (!this.getLabyrinthe().estUnMur(x, y) && !this.getLabyrinthe().estUnPiege(x, y) && !this.getLabyrinthe().estUnePotion(x,y) &&!this.getLabyrinthe().estUnPnj(x, y)) this.setPosition(x, y);
	}

	/**
	 * getDirection, donne la direction du tir
	 * @return direction du tir
	 */
	public int getDirection() {
		return this.direction;
	}

	/**
	 * methode permettant de retounrner le numero attribuer au monstre
	 * @return 3
	 */
	@Override
	public String toString() {
		return "3";
	}

}
