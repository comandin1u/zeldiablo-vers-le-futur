package MonJeu;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

public class Sprites {

	/**
	 * attribut sprite de type Map
	 */
	public Map<String, BufferedImage> sprites;


	/**
	 * constrcuteur de la classe Sprite
	 */
	public Sprites() {
		this.sprites = new HashMap<String, BufferedImage>();
		try {
			this.sprites.put("MUR", ImageIO.read(new File("sprites/mur.jpg")));
			this.sprites.put("SOL", ImageIO.read(new File("sprites/sol.jpg")));
			this.sprites.put("SORTIE", ImageIO.read(new File("sprites/sortie.jpg")));
			this.sprites.put("PIEGE", ImageIO.read(new File("sprites/piege.jpg")));
			this.sprites.put("POTION", ImageIO.read(new File("sprites/potion.png")));
			this.sprites.put("TIRG", ImageIO.read(new File("sprites/tirG.png")));
			this.sprites.put("TIRD", ImageIO.read(new File("sprites/tirD.png")));
			this.sprites.put("TIRB", ImageIO.read(new File("sprites/tirB.png")));
			this.sprites.put("TIRH", ImageIO.read(new File("sprites/tirH.png")));
			this.sprites.put("MONSTRE", ImageIO.read(new File("sprites/monstre.png")));
			this.sprites.put("MONSTRED", ImageIO.read(new File("sprites/monstreD.png")));
			this.sprites.put("MONSTREG", ImageIO.read(new File("sprites/monstreG.png")));
			this.sprites.put("MONSTREH", ImageIO.read(new File("sprites/monstreH.png")));
			this.sprites.put("AVENTURIER", ImageIO.read(new File("sprites/aventurier.png")));
			this.sprites.put("AVENTURIERD", ImageIO.read(new File("sprites/aventurierD.png")));
			this.sprites.put("AVENTURIERG", ImageIO.read(new File("sprites/aventurierG.png")));
			this.sprites.put("AVENTURIERH", ImageIO.read(new File("sprites/aventurierH.png")));
			this.sprites.put("PNJ", ImageIO.read(new File("sprites/pnj.png")));
			this.sprites.put("GAMEOVER", ImageIO.read(new File("sprites/gameOver.png")));
			this.sprites.put("YOUWIN", ImageIO.read(new File("sprites/youWin.png")));
			
			this.sprites.put("BOSSHD", ImageIO.read(new File("sprites/bosshd.png")));
			this.sprites.put("BOSSHG", ImageIO.read(new File("sprites/bosshg.png")));
			this.sprites.put("BOSSBD", ImageIO.read(new File("sprites/bossbd.png")));
			this.sprites.put("BOSSBG", ImageIO.read(new File("sprites/bossbg.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * methode retournant l image du sprite
	 * @param img
	 * @return BufferedImage
	 */
	public BufferedImage getImage(String img) {
		return this.sprites.get(img);
	}
}

