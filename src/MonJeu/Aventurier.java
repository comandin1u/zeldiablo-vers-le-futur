
package MonJeu;

import moteurJeu.Commande;

/**
 * @author Steven
 *
 */
public class Aventurier extends Personnage {

	/**
	 * attribut direction
	 */
	private int direction;

	/**
	 * attribut tir
	 */
	private Tir tir;

	/**
	 * constructeur de la classe Aventurier
	 * @param dx
	 * @param dy
	 * @param lab
	 */
	public Aventurier(int dx, int dy, Labyrinthe lab){
		super(dx, dy, lab);
		this.tir = null;
		this.direction = 3;
	}

	/**
	 * methode permettant de deplacer le personnage avec une commande passee en paramtere
	 * @param c 
	 */
	public void seDeplacer(Commande c){
		int x = this.getX();
		int y = this.getY();

		if (c.haut) {
			this.direction = 0;
			y--;
		}

		if (c.gauche) {
			this.direction = 2;
			x--;
		}

		if (c.bas) {
			this.direction = 1;
			y++;
		}

		if (c.droite) {
			this.direction = 3;
			x++;
		}

		if (x < 0 || y < 0) return;

		// boucle For permettant de diviser les points de vie du personnages par 2 en parcourant la liste de monstre
		for(Monstre m : this.getLabyrinthe().getMonstre()){
			if(m.getX() == this.getX() && m.getY() == this.getY()){
				this.setPdv(this.getPdv() - 10);
			}
		}
		
		//degats du boss au CAC
		for(Boss b : this.getLabyrinthe().getBoss()){
			if(b.getX() == this.getX() && b.getY() == this.getY()){
				this.setPdv(this.getPdv() - 1);
			}
			if(b.getX() == this.getX() && b.getY()-1 == this.getY()){
				this.setPdv(this.getPdv() - 1);
			}
			if(b.getX()-1 == this.getX() && b.getY() == this.getY()){
				this.setPdv(this.getPdv() - 1);
			}
			if(b.getX()-1 == this.getX() && b.getY()-1 == this.getY()){
				this.setPdv(this.getPdv() - 1);
			}
		}

		int niv = this.getLabyrinthe().getNiveaux().getLvlEnCours();
		for(Piege p : this.getLabyrinthe().getPiege()){
			if(p.getX() == this.getX() && p.getY() == this.getY()){
				this.getLabyrinthe().getNiveaux().decrementerNiveaux();
			}
		}
		int indexPotion = -1;
		for(Potion po : this.getLabyrinthe().getPotion()){
			int index = -1;
			if(po.getX() == this.getX() && po.getY() == this.getY()){
				if(this.getPdv() < 100){
					this.setPdv(this.getPdv() + 10);
					indexPotion = this.getLabyrinthe().getPotion().indexOf(po);
				}
			}
		}
		if (indexPotion > -1) this.getLabyrinthe().getPotion().remove(indexPotion);


		if (this.getLabyrinthe().getNiveaux().getLvlEnCours() < niv) {
			this.getLabyrinthe().contruireLabyrinthe(this.getLabyrinthe().getNiveaux().getLvlEnCours());
		}

		//si les coordonnes de l aventurier sont egales a celle du mur alors l aventurier ne superpose pas avec le mur 
		if (this.getX() == this.getLabyrinthe().getSortie()[0] && this.getY() == this.getLabyrinthe().getSortie()[1]) {
			this.getLabyrinthe().getNiveaux().incrementerNiveaux();
			this.getLabyrinthe().contruireLabyrinthe(this.getLabyrinthe().getNiveaux().getLvlEnCours());
		} else if (!this.getLabyrinthe().estUnMur(x, y) &&!this.getLabyrinthe().estUnPnj(x, y)) super.setPosition(x, y);

		if(c.tirer && this.tir == null){
			this.tir = new Tir(this.getX(), this.getY(), this.getDirection());
		}
	}

	/**
	 * methode permettant d afficher le numero attribue a l aventurier
	 * @return 2
	 */
	@Override
	public String toString() {
		return "2";
	}

	/**
	 * methode permettant de retourner un tir
	 */
	public Tir getTir(){
		return this.tir;
	}

	/**
	 * destruction du tir 
	 */
	public void detruireTir() {
		this.tir = null;
	}

	/**
	 * Obtention de la direction du tir
	 * @return direction du tir
	 */
	public int getDirection(){
		return this.direction;
	}


}
