package MonJeu;

public class Potion extends Objet {

	/**
	 * attribut labyrinthe de type Labyrinthe
	 */
	private Labyrinthe labyrinthe;

	/**
	 * constructeur de la classe Potion prenant deux parametres de type entier et un parametre de type Labyrinthe
	 * @param dx
	 * @param dy
	 * @param lab
	 */
	public Potion(int dx, int dy, Labyrinthe lab){
		super(dx,dy, lab);
	}


	/**
	 * methode toString retournant le numero attribue a la potion de type string
	 * @return 6
	 */
	@Override
	public String toString() {
		return "6";
	}
}