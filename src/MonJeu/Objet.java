package MonJeu;

public abstract class Objet {

	/**
	 * attribut x et y de type entier
	 */
	private int x,y;
	
	/**
	 * attribut labyrinthe
	 */
	private Labyrinthe labyrinthe;

	/**
	 * constructeur de la classe Objet
	 * @param dx
	 * @param dy
	 */
	public Objet(int dx, int dy,Labyrinthe lab){
		this.x = dx;
		this.y = dy;
		this.labyrinthe = lab;
	}

	/**
	 * methode abstract retournant un String
	 */
	public abstract String toString();

	/**
	 * methode retournant l attribut x
	 * @return x
	 */
	public int getX(){
		return this.x;
	}

	/**
	 * methode changeant l attribut x
	 * @param x
	 */
	public void setX(int x) {
		this.x = x;
	}
	
	/**
	 * methode retournant l attribut y
	 * @return y
	 */
	public int getY(){
		return this.y;
	}

	/**
	 * methode changeant l attribut y
	 * @param y
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * methode changeant les valeurs de la position
	 * @param x
	 * @param y
	 */
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * methode permettant de retourner le Labyrinthe
	 * @return Labyrinthe
	 */
	public Labyrinthe getLabyrinthe(){
		return this.labyrinthe;
	}
}
