package MonJeu;

public class Piege extends Objet {
	
	/**
	 * constructeur de la classe Piege prenant 3 parametres de type int et Labyrinthe
	 * @param dx
	 * @param dy
	 * @param lab
	 */
	public Piege(int dx, int dy, Labyrinthe lab) {
		super(dx, dy, lab);
	}
	
	/**
	 * methode toString retournant 4 qui est le numero attribue au piege
	 */
	@Override
	public String toString() {
		return "4";
	}
	
}
