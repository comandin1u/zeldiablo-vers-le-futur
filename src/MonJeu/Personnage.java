package MonJeu;

import moteurJeu.Commande;

public abstract class Personnage {

	/**
	 * attribut pdv de type entier
	 */
	private int pdv;

	/**
	 * attribut x de type entier
	 */
	private int x;

	/**
	 * attribut y de type entier
	 */
	private int y;

	/**
	 * attribut labyrinthe
	 */
	private Labyrinthe labyrinthe;

	/**
	 * constructeur de la classe Personnage ne prenant pas de parametres
	 */
	public Personnage(){
		this.pdv = 100;
		this.x = 0;
		this.y = 0;
		this.labyrinthe = new Labyrinthe();
	}

	/**
	 * constructeur de la classe Personnage prenant deux parametres de type entier
	 * @param dx
	 * @param dy
	 */
	public Personnage(int dx, int dy, Labyrinthe lab){
		this.pdv = 100;
		this.x = dx;
		this.y = dy;
		this.labyrinthe = lab;
	}

	/**
	 * methode abstract seDeplacer permettant a un personnage de se deplacer grace a une commande passee en parametre
	 * @param c
	 */
	public abstract void seDeplacer(Commande c);

	/**
	 * methode abstract toString retournant un String
	 */
	public abstract String toString();

	/**
	 * methode retournant l attribut x
	 * @return x
	 */
	public int getX(){
		return this.x;
	}

	/**
	 * methode changeant l attribut x
	 * @param x
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * methode retournant l attribut y
	 * @return y
	 */
	public int getY(){
		return this.y;
	}

	/**
	 * methode changeant la valeur de l attribut y
	 * @param y
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * methode permettant de retourner les points de vie du personnage
	 * @return pdv
	 */
	public int getPdv(){
		return this.pdv;
	}

	/**
	 * methode permettant de modifier la valeur de l attribut pdv de type entier
	 * @param pdv
	 */
	public void setPdv(int pdv) {
		this.pdv = pdv;
	}

	/**
	 * methode permettant de retourner le Labyrinthe
	 * @return Labyrinthe
	 */
	public Labyrinthe getLabyrinthe(){
		return this.labyrinthe;
	}

	/**
	 * methode permettant de modifier la position du personnage
	 * @param x
	 * @param y
	 */
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

}

