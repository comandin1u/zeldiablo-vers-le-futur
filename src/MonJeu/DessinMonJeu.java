package MonJeu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import moteurJeu.DessinJeu;

public class DessinMonJeu implements DessinJeu {

	/**
	 *  attribut static permettant d initialiser une valeur a taille_case
	 */
	public static final int TAILLE_CASE = 25;

	/**
	 * attribut Jeu
	 */
	private Jeu jeu;

	/**
	 * Constructeur d'affichage du jeu
	 * @param Jeu � afficher au joueur
	 */
	public DessinMonJeu(Jeu jeu) {
		this.jeu = jeu;
	}

	/**
	 * 
	 * methode dessiner permettant d afficher le labyrinthe, l aventurier, les monstres et les objets
	 * @param image
	 */
	@Override
	public void dessiner(BufferedImage image) {
		for (int x = 0; x < this.jeu.getLabyrinthe().getLabyrinthe().length; x++) {
			for (int y = 0; y < this.jeu.getLabyrinthe().getLabyrinthe()[x].length; y++) {
				if (this.jeu.getLabyrinthe().estUnMur(x, y)) {
					this.dessinerObjet("MUR", x, y, image);
				}else if (!this.jeu.getLabyrinthe().estUnMur(x, y)) {
					this.dessinerObjet("SOL", x, y, image);
				}
			}
		}
		for(Piege p : this.jeu.getLabyrinthe().getPiege()) {
			this.dessinerObjet("PIEGE", p.getX(), p.getY(), image);
		}
		for(Monstre mo : this.jeu.getLabyrinthe().getMonstre()) {
			this.dessinerObjet("MONSTRE", mo.getX(), mo.getY(), image);
		}
		this.dessinerObjet("SORTIE", this.jeu.getLabyrinthe().getSortie()[0], this.jeu.getLabyrinthe().getSortie()[1], image);
		
		for(Potion po : this.jeu.getLabyrinthe().getPotion()) {
			this.dessinerObjet("POTION", po.getX(), po.getY(), image);
		}
		
		for(Pnj pn : this.jeu.getLabyrinthe().getPnj()){
			this.dessinerObjet("PNJ", pn.getX(), pn.getY(), image);
		}
		
		for(Boss b : this.jeu.getLabyrinthe().getBoss()){
			this.dessinerObjet("BOSSBD", b.getX()-1, b.getY(), image);
			this.dessinerObjet("BOSSBG", b.getX(), b.getY(), image);
			this.dessinerObjet("BOSSHG", b.getX(), b.getY()-1, image);
			this.dessinerObjet("BOSSHD", b.getX()-1, b.getY()-1, image);
		}
		
		this.dessinerObjet("AVENTURIER", this.jeu.getLabyrinthe().getAventurier().getX(), this.jeu.getLabyrinthe().getAventurier().getY(), image);
		
		if(this.jeu.getLabyrinthe().getAventurier().getTir() != null){
			this.dessinerObjet("TIR", this.jeu.getLabyrinthe().getAventurier().getTir().getX(), this.jeu.getLabyrinthe().getAventurier().getTir().getY(), image);
		}
		
		this.dessinerObjet("LEVEL", 25, 20, image);


		if (this.jeu.etreFini()) {
			this.dessinerObjet("FIN", 0, 0, image);
		}

	}

	/**
	 * methode dessinerObjet permettant de dessiner les objets labyrinthe, aventurier, monstres et  
	 * @param str, x, y, img
	 */
	private void dessinerObjet(String str, int x, int y, BufferedImage img) {
		Graphics2D g = (Graphics2D) img.getGraphics();

		switch (str) {
			case "AVENTURIER":
				String spriteAventurier;
				switch (this.jeu.getLabyrinthe().getAventurier().getDirection()) {
					case 0:
						spriteAventurier = "AVENTURIERH";
						break;
						
					case 1:
						spriteAventurier = "AVENTURIER";
						break;
						
					case 2:
						spriteAventurier = "AVENTURIERG";
						break;
						
					case 3:
						spriteAventurier = "AVENTURIERD";
						break;
					
					default:
						spriteAventurier = "AVENTURIER";
						break;
				}
				
				g.drawImage(this.jeu.getSprites().getImage(spriteAventurier), (x * TAILLE_CASE), (y * TAILLE_CASE), 25, 25, null);
				
				g.setColor(Color.WHITE);
				g.drawString("Vie : ", 255, 20);
				
				g.setColor(Color.DARK_GRAY);
				g.fillRect(280, 8, 102, 15);
				g.setColor(Color.YELLOW);
				g.fillRect(281, 9, this.jeu.getLabyrinthe().getAventurier().getPdv(), 13);
				g.setColor(Color.MAGENTA);
				String vie = String.format("%d", this.jeu.getLabyrinthe().getAventurier().getPdv());
				g.drawString(vie, (379 - g.getFontMetrics().stringWidth(vie)), 20);
				break;
				
				
			
				
				
	
			case "MONSTRE":
				String spriteMonstre = "MONSTRE";
				for (Monstre mo : this.jeu.getLabyrinthe().getMonstre()) {
					switch (mo.getDirection()) {
						case 0:
							spriteMonstre = "MONSTREH";
							break;
							
						case 1:
							spriteMonstre = "MONSTRE";
							break;
							
						case 2:
							spriteMonstre = "MONSTREG";
							break;
							
						case 3:
							spriteMonstre = "MONSTRED";
							break;
						
						default:
							spriteMonstre = "MONSTRE";
							break;
					}
				}
				g.drawImage(this.jeu.getSprites().getImage(spriteMonstre), (x * TAILLE_CASE), (y * TAILLE_CASE), 25, 25, null);
				break;
	
			case "MUR":
				g.drawImage(this.jeu.getSprites().getImage("MUR"), (x * TAILLE_CASE), (y * TAILLE_CASE), 25, 25, null);
				break;
	
			case "SOL":
				g.drawImage(this.jeu.getSprites().getImage("SOL"), (x * TAILLE_CASE), (y * TAILLE_CASE), 25, 25, null);
				break;
	
			case "PIEGE":
				g.drawImage(this.jeu.getSprites().getImage("PIEGE"), (x * TAILLE_CASE), (y * TAILLE_CASE), 25, 25, null);
				break;
	
			case "TIR":
				switch (this.jeu.getLabyrinthe().getAventurier().getDirection()) {
				case 0:
					g.drawImage(this.jeu.getSprites().getImage("TIRH"), (x * TAILLE_CASE), (y * TAILLE_CASE), 25, 25, null);
					break;
					
				case 1:
					g.drawImage(this.jeu.getSprites().getImage("TIRB"), (x * TAILLE_CASE), (y * TAILLE_CASE), 25, 25, null);
					break;
					
				case 2:
					g.drawImage(this.jeu.getSprites().getImage("TIRG"), (x * TAILLE_CASE), (y * TAILLE_CASE), 25, 25, null);
					break;
					
				case 3:
					g.drawImage(this.jeu.getSprites().getImage("TIRD"), (x * TAILLE_CASE), (y * TAILLE_CASE), 25, 25, null);
					break;
				}
				break;
	
			case "SORTIE":
				g.drawImage(this.jeu.getSprites().getImage("SORTIE"), (x * TAILLE_CASE), (y * TAILLE_CASE), 25, 25, null);
				break;
	
			case "LEVEL":
				g.setColor(Color.WHITE);
				g.drawString(String.format("Niveau : %d", this.jeu.getLabyrinthe().getNiveaux().getLvlEnCours()), x, y);
				break;
	
			case "POTION":
				g.drawImage(this.jeu.getSprites().getImage("POTION"), (x * TAILLE_CASE), (y * TAILLE_CASE), 25, 25, null);
				break;
				
			case "PNJ":
				g.drawImage(this.jeu.getSprites().getImage("PNJ"), (x * TAILLE_CASE), (y * TAILLE_CASE), 25, 25, null);
				
				for(Pnj p : this.jeu.getLabyrinthe().getPnj()){
					
					if(p.showDialogue == true){
					
						g.setColor(Color.WHITE);
						g.drawString(p.getTS()[/*this.jeu.getLabyrinthe().getPnj().indexOf(p)*/0], (x * TAILLE_CASE), (y * TAILLE_CASE)-10);
						//p.setDialogue(null);
					}	
					
				}
				
				break;
				
			case "BOSSBG":
				g.drawImage(this.jeu.getSprites().getImage("BOSSBD"), (x * TAILLE_CASE), (y * TAILLE_CASE), 25, 25, null);
				g.drawImage(this.jeu.getSprites().getImage("BOSSHD"), (x * TAILLE_CASE), ((y-1) * TAILLE_CASE), 25, 25, null);
				g.drawImage(this.jeu.getSprites().getImage("BOSSBG"), ((x-1) * TAILLE_CASE), (y * TAILLE_CASE), 25, 25, null);
				g.drawImage(this.jeu.getSprites().getImage("BOSSHG"), ((x-1) * TAILLE_CASE), ((y-1) * TAILLE_CASE), 25, 25, null);
				break;
	
			case "FIN":
				if (this.jeu.getLabyrinthe().getAventurier().getPdv() <= 0) {
					g.drawImage(this.jeu.getSprites().getImage("GAMEOVER"), x, y, TAILLE_CASE * this.jeu.getLabyrinthe().getLabyrinthe().length, TAILLE_CASE * this.jeu.getLabyrinthe().getLabyrinthe().length, null);
				} else {
					g.drawImage(this.jeu.getSprites().getImage("YOUWIN"), x, y, TAILLE_CASE * this.jeu.getLabyrinthe().getLabyrinthe().length, TAILLE_CASE * this.jeu.getLabyrinthe().getLabyrinthe().length, null);
				}
				break;
		}

		g.dispose();
	}

}
