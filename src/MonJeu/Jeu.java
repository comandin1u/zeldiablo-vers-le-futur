package MonJeu;

import moteurJeu.Commande;

public class Jeu implements moteurJeu.Jeu {

	
	public static int IDPNJ = 1;
	/**
	 * attribut labyrinthe 
	 */
	private Labyrinthe labyrinthe;
	
	/**
	 * fin du jeu false de base
	 */
	private boolean estFini;
	
	/**
	 * attribut de sprites 
	 */
	private Sprites sprites;

	/**
	 * constructeur de la classe labyrinthe
	 */
	public Jeu() {
		this.labyrinthe = new Labyrinthe();
		this.estFini = false;
		this.sprites = new Sprites();
	}
	
	/**
	 * methode tostring permettant d afficher le labyrinthe
	 * @return labyrinthe.toString()
	 */
	public String toString() {
		return this.labyrinthe.toString();
	}

	/**
	 * methode evoluer permettant de faire deplacer l aventurier en fonction des commandes
	 * @param commande
	 */
	@Override
	public void evoluer(Commande commande) {
		this.labyrinthe.getAventurier().seDeplacer(commande);
		
		if (this.labyrinthe.getAventurier().getPdv() <= 0) this.estFini();
		
		if (this.labyrinthe.getSortie()[0] == this.labyrinthe.getAventurier().getX() && this.labyrinthe.getSortie()[1] == this.labyrinthe.getAventurier().getY()) {
			if (this.labyrinthe.getNiveaux().getLvlEnCours() == this.labyrinthe.getNiveaux().getLvl().size()) {
				this.estFini();
			}
		}
		
		for (Monstre m : this.labyrinthe.getMonstre()) {
			m.seDeplacer(null);
		}
		
		for (Boss b : this.labyrinthe.getBoss()) {
			b.seDeplacer(null);
		}
		
		if(this.labyrinthe.getAventurier().getTir() != null){
			this.labyrinthe.evoluerTir();
		}
	}

	/**
	 * methode est fini retournant faux
	 * @return false ou true si jeu fini ou non
	 */
	@Override
	public boolean etreFini() {
		return this.estFini;
	}
	
	/**
	 * passe estFini a vrai 
	 */
	public void estFini() {
		this.estFini = true;
		this.labyrinthe.supprimerEntite();
	}

	/**
	 * methode getLabyrinthe() retournant un labyrinthe 
	 * @return labyrinthe
	 */
	public Labyrinthe getLabyrinthe() {
		return this.labyrinthe;
	}
	
	/**
	 * methode getSprites() retournant les sprites 
	 * @return sprites
	 */
	public Sprites getSprites() {
		return this.sprites;
	}

}
