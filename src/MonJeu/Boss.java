package MonJeu;

import java.util.Random;

import moteurJeu.Commande;

public class Boss extends Personnage {

	private int direction;
	
	private int pdv;
	
	public Boss(int dx, int dy, Labyrinthe l){
		super(dx, dy, l);
		this.direction = 1;
		this.pdv = 100;
	}
	
	@Override
	public void seDeplacer(Commande c) {
		int direction = new Random().nextInt((4 - 0) + 1);
		this.deplacement(direction);
		
		//A CHANGER
	}
	
	public void deplacement(int direction) {
		int x = this.getX();
		int y = this.getY();
		switch (direction) {
		case 0:
			x--;
			this.direction = 0;
			break;

		case 1:
			x++;
			this.direction = 1;
			break;

		case 2:
			y--;
			this.direction = 2;
			break;

		case 3:
			y++;
			this.direction = 3;
			break;
		}
		if (x == this.getLabyrinthe().getSortie()[0] && y == this.getLabyrinthe().getSortie()[1]) return;

		if (
				!this.getLabyrinthe().estUnMur(x, y) 
				&& !this.getLabyrinthe().estUnMur(x, y-1)	
				&& !this.getLabyrinthe().estUnMur(x-1, y)	
				&& !this.getLabyrinthe().estUnMur(x-1, y-1)	
				
				&& !this.getLabyrinthe().estUnPiege(x, y)
				&& !this.getLabyrinthe().estUnPiege(x, y-1)	
				&& !this.getLabyrinthe().estUnPiege(x-1, y)	
				&& !this.getLabyrinthe().estUnPiege(x-1, y-1)	
				
				&& !this.getLabyrinthe().estUnePotion(x,y)
				&& !this.getLabyrinthe().estUnePotion(x, y-1)	
				&& !this.getLabyrinthe().estUnePotion(x-1, y)	
				&& !this.getLabyrinthe().estUnePotion(x-1, y-1)	
			) 
			this.setPosition(x, y);
	}

	public int getDirection() {
		return this.direction;
	}
	
	@Override
	public String toString() {
		return "8";
	}

}
