package MonJeu;
import MonJeu.Jeu;

import java.util.ArrayList;
import java.util.Random;

/**
 * Classe Labyrinthe creant un labyrinthe
 */
public class Labyrinthe {

	/**
	 * attribut labyrinthe
	 */
	private Integer [][] labyrinthe;
	
	/**
	 * attribut sortie 
	 */
	private int[] sortie;
	
	/**
	 * attribut aventurier
	 */
	private Aventurier aventurier;
	
	/**
	 * attribut monstre
	 */
	private ArrayList<Monstre> monstre;
	
	/**
	 * attribut potion
	 */
	private ArrayList<Potion> potion;
	
	/**
	 * attribut piege
	 */
	private ArrayList<Piege> piege;
	
	private ArrayList<Pnj> pnj;
	
	private ArrayList<Boss> boss;
	
	/**
	 * attribut niveaux
	 */
	private Niveaux niveaux;
	
	
	/**
	 * constructeur de la classe labyrinthe
	 */
	public Labyrinthe(){
		this.niveaux = new Niveaux();
		this.potion = new ArrayList<Potion>();
		this.monstre = new ArrayList<Monstre>();
		this.piege = new ArrayList<Piege>();
		this.pnj = new ArrayList<Pnj>();
		this.boss = new ArrayList<Boss>();
		this.labyrinthe = new Integer [16][16];
		for(int i = 0; i < this.labyrinthe.length; i++){
			for(int j = 0; j < this.labyrinthe[i].length; j++){
				this.labyrinthe[i][j] = -1;
			}
		}
		this.sortie = new int[2];
		this.contruireLabyrinthe(1);
	}

	/**
	 * methode permettant de pouvoir construire un labyrinthe et d y ajouter l aventurier, le monstre 
	 * et les pieges
	 * @param niv
	 */
	public void contruireLabyrinthe(int niv){
		this.supprimerEntite();
		this.labyrinthe = this.niveaux.getLvl().get(niv);
		for (int i = 0; i < this.labyrinthe.length; i++) {
			for (int j = 0; j < this.labyrinthe[i].length; j++) {
				if (this.labyrinthe[i][j] == 2) {
					if (this.aventurier == null) {
						this.ajouterAventurier(new Aventurier(i, j, this));
					} else {
						int pdv = this.aventurier.getPdv();
						this.ajouterAventurier(new Aventurier(i, j, this));
						this.aventurier.setPdv(pdv);
					}
				}
				
				if (this.labyrinthe[i][j] == 3) {
					this.ajouterMonstre(new Monstre(i, j, this));
				}
				
				if (this.labyrinthe[i][j] == 4) {
					this.ajouterPiege(new Piege(i, j, this));
				}
				
				if (this.labyrinthe[i][j] == 6){
					this.ajouterPotion(new Potion(i, j, this));
				}
				
				if (this.labyrinthe[i][j] == 5) {
					this.sortie[0] = i;
					this.sortie[1] = j;
				}
				
				if (this.labyrinthe[i][j] == 7) {
					Pnj pnj = new Pnj(i,j,this,Jeu.IDPNJ);
					Jeu.IDPNJ++;
					System.out.println(Jeu.IDPNJ);
					PnjDialogue pdial = new PnjDialogue();
					pdial.setDialogue(pnj);
					
					this.ajouterPnj(pnj);
				}
				
				if (this.labyrinthe[i][j] == 8) {
					this.ajouterBoss(new Boss(i,j,this));
				}
			}
		}
	}

	/**
	 * methode permettant de retourner un labyrinthe
	 * @return labyrinthe
	 */
	public Integer [][] getLabyrinthe(){
		return this.labyrinthe;
	}
	
	/**
	 * methode permettant de retourner l attribut sortie
	 * @return sortie
	 */
	public int[] getSortie() {
		return this.sortie;
	}
	
	/**
	 * methode permettant de modifier le labyrinthe
	 * @param i position X
	 * @param j position Y
	 * @param p indice de l'entite
	 */
	public void setLabyrinthe(int i, int j, int p){
		this.labyrinthe[i][j] = p;
	}

	/**
	 * methode permettant de savoir si la position est un mur
	 * @param x position x
	 * @param y position y
	 * @return true ou false si c'est un mur ou non
	 */
	public boolean estUnMur(int x, int y){
		if(this.labyrinthe[x][y] == 1) return true;
		return false;
	}
	
	/**
	 * methode permettant de savoir si la position est un piege
	 * @param x position x
	 * @param y position y
	 * @return true ou false si c'est un piege ou non
	 */
	public boolean estUnPiege(int x, int y) {
		if(this.labyrinthe[x][y] == 4) return true;
		return false;
	}
	
	public boolean estUnePotion(int x, int y){
		if(this.labyrinthe[x][y] == 6) return true;
		return false;
	}
	
	public boolean estUnPnj(int x, int y){
		if(this.labyrinthe[x][y] == 7) return true;
		return false;
	}
	
	public boolean estUnBoss(int x, int y){
		if(this.labyrinthe[x][y] == 8) return true;
		return false;
	}
	
	/**
	 * methode permettant de retourner un aventurier
	 * @return aventurier
	 */
	public Aventurier getAventurier() {
		return this.aventurier;
	}
	
	/**
	 * methode permettant de retourner un piege
	 * @return piege
	 */
	public ArrayList<Piege> getPiege(){
		return this.piege;
	}
	
	/**
	 * methode permettant de retourner une potion
	 * @return potion
	 */
	public ArrayList<Potion> getPotion(){
		return this.potion;
	}
	
	public ArrayList<Pnj> getPnj() {
		return this.pnj;
	}

	/**
	 * methode permettant de retourner un monstre
	 * @return monstre
	 */
	public ArrayList<Monstre> getMonstre(){
		return this.monstre;
	}
	
	public ArrayList<Boss> getBoss(){
		return this.boss;
	}
	
	/**
	 * methode permettant d ajouter un aventurier passe en parametre
	 * @param av aventurier
	 */
	public void ajouterAventurier(Aventurier av) {
		this.aventurier = av;
	}
	
	/**
	 * methode permettant d ajouter un monstre passe en parametre
	 * @param m monstre
	 */
	public void ajouterMonstre(Monstre m) {
		this.monstre.add(m);
	}
	
	/**
	 * methode permettant d ajouter un piege passe en parametre 
	 * @param p piege
	 */
	public void ajouterPiege(Piege p) {
		this.piege.add(p);
	}
	
	/**
	 * methode permettant d ajouter une potion passe en parametre
	 * @param po potion
	 */
	public void ajouterPotion(Potion po){
		this.potion.add(po);
	}
	
	public void ajouterPnj(Pnj pn) {
		this.pnj.add(pn);
	}
	
	public void ajouterBoss(Boss b) {
		this.boss.add(b);
	}
	
	/**
	 * methode permettant de supprimer une entite
	 */
	public void supprimerEntite() {
		if (!this.monstre.isEmpty()) this.monstre.clear();
		if (!this.piege.isEmpty()) this.piege.clear();
		if (!this.potion.isEmpty()) this.potion.clear();
		if (!this.pnj.isEmpty()) this.pnj.clear();
		if (!this.boss.isEmpty()) this.boss.clear();
		
	}
	
	/**
	 * methode retournant les niveaux
	 * @return niveaux
	 */
	public Niveaux getNiveaux(){
		return this.niveaux;
	}

	/**
	 * Evolution du tir (deplacement) 
	 */
	public void evoluerTir() {
		Tir tir = this.aventurier.getTir();
		int x = tir.getX();
		int y = tir.getY();
		
		switch(tir.getDirection()){
		case 0 :
			y--;
			break;
			
		case 1 :
			y++;
			break;
			
		case 2 :
			x--;
			break;
			
		case 3 :
			x++;
			break;
		}
		
		if(!this.estUnMur(x, y)) tir.setPosition(x, y);
		if(this.estUnMur(x, y) || this.sortie[0] == tir.getX() && this.sortie[1] == tir.getY()) this.aventurier.detruireTir();
		
		int index = -1;
		for(Monstre m : this.monstre){
			if(m.getX() == tir.getX() && m.getY() == tir.getY()) {
				this.aventurier.detruireTir();
				index = this.monstre.indexOf(m);
			}
		}
		if(index > -1){
			this.monstre.remove(index);
			index = -1;
			
		}
		
		int index2 = -1;
		for(Boss b : this.boss){
			//A ameliorer QUAND MEME
			if(b.getX() == tir.getX()-1 && b.getY() == tir.getY()) {
				this.aventurier.detruireTir();
				this.boss.get(this.boss.indexOf(b)).setPdv(this.boss.get(this.boss.indexOf(b)).getPdv()-20);
			}
			if(b.getX()-1 == tir.getX() && b.getY() == tir.getY()) {
				this.aventurier.detruireTir();
				this.boss.get(this.boss.indexOf(b)).setPdv(this.boss.get(this.boss.indexOf(b)).getPdv()-20);
			}
			if(b.getX() == tir.getX() && b.getY()-1 == tir.getY()) {
				this.aventurier.detruireTir();
				this.boss.get(this.boss.indexOf(b)).setPdv(this.boss.get(this.boss.indexOf(b)).getPdv()-20);
			}
			if(b.getX()-1 == tir.getX()-1 && b.getY()-1 == tir.getY()) {
				this.aventurier.detruireTir();
				this.boss.get(this.boss.indexOf(b)).setPdv(this.boss.get(this.boss.indexOf(b)).getPdv()-20);
			}
	
			if(this.boss.get(0).getPdv() <= 0){
				index2 = this.boss.indexOf(b);
			}
		}
		if(index2 > -1){
			this.boss.remove(index2);
			index2 = -1;
			
		}
		
		for(Pnj p : this.pnj){
			
			if(p.getX() == tir.getX() && p.getY() == tir.getY()) {
				
				if(p.getDialogue() == null){
					p.setDialogue(p.getTS()[this.pnj.indexOf(p)]);
					
					if(p.showDialogue == false){
						p.showDialogue = true;
					}
					else{
						p.showDialogue = false;
					}				
				}
				else{
					p.setDialogue(null);
				}

		
				this.aventurier.detruireTir();
			}
		}
		
		
	}

}
