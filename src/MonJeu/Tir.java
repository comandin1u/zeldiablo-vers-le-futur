package MonJeu;

public class Tir {

	/**
	 * attribut de la classe tir
	 */
	private int x,y, direction;
	
	/**
	 * constructeur de la classe tir
	 */
	public Tir(int dx,int dy, int direction){
		this.x = dx;
		this.y = dy;
		this.direction = direction;
	}
	
	/**
	 * methode retournant la direction du tir
	 * @return direction
	 */
	public int getDirection() {
		return this.direction;
	}
	
	/**
	 * methode retournant l attribut x
	 * @return x
	 */
	public int getX(){
		return this.x;
	}
	
	
	/**
	 * Modification de la position du tir
	 * @param dx poisition X
	 * @param dy position Y
	 */
	public void setPosition(int dx, int dy){
		this.x = dx;
		this.y = dy;
	}
	
	/**
	 * methode retournant l attribut y
	 * @return y
	 */
	public int getY(){
		return this.y;
	}
	
	/**
	 * methode lancerTir
	 */
	public boolean lancerTir(){
		 x++;
		 if(this.x <= 16){
			 return false;
		 } else {
			 return true;
		 }
	}
}
