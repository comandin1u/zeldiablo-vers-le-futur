
package MonJeu;

import java.io.BufferedReader;

import Editeur.Editeur;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import Editeur.Editeur;

/**
 * Classe Niveaux, g�rant les diff�rents niveaux
 */
public class Niveaux {
	
	public static int nbr_niveau_default = 1;

	private int nbr_niveaux = nbr_niveau_default;

	/**
	 * attribut de type Map Integer
	 */
	private Map<Integer, Integer[][]> lvl;

	/**
	 * attribut representant le niveau en cours
	 */
	private int lvl_en_cours;

	/**
	 * premier niveau
	 */
	private Integer[][] NIVEAU_1 = {
			{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			{1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1},
			{1, 1, 0, 3, 1, 1, 1, 0, 8, 0, 1, 1, 0, 3, 1, 1},
			{1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1},
			{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			{1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0},
			{1, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 5},
			{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			{1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1},
			{1, 1, 0, 0, 1, 1, 1, 0, 8, 0, 1, 1, 0, 0, 1, 1},
			{1, 1, 0, 3, 1, 1, 1, 0, 0, 0, 1, 1, 0, 3, 1, 1},
			{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
	};
	
	/**
	 * constructeur de la classe niveau
	 */
	public Niveaux(){
		this.lvl_en_cours = 1;
		this.lvl = new HashMap<Integer, Integer[][]>();
		lvl.put(1, NIVEAU_1);
		this.creerCustomLvl();
	}

	/**
	 * methode retournant l attribut lvl
	 * @return lvl
	 */
	public Map<Integer, Integer[][]> getLvl(){
		return lvl;
	}

	/**
	 * methode retournant le niveau en cours
	 * @return lvl_en_cours
	 */
	public int getLvlEnCours(){
		return this.lvl_en_cours;
	}

	/**
	 * methode permettant d incrementer un niveau
	 */
	public void incrementerNiveaux(){
		if(this.lvl_en_cours < this.lvl.size()) this.lvl_en_cours++;
	}

	/**
	 * * methode permettant de decrementer un niveau
	 */
	public void decrementerNiveaux() {
		if (this.lvl_en_cours > 1) this.lvl_en_cours--;
	}

	/**
	 * creerCustomLvl remplace le niveau 2 par le niveau custpmisable s'il existe
	 */
	public void creerCustomLvl(){
		try{
			File repertoire = new File("niveaux");
			this.listerRepertoire(repertoire);

			for(int o=0; o<(nbr_niveaux-nbr_niveau_default); o++){
				
				Integer[][] Custom = {
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
						{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				};

				File ff=new File("niveaux/NiveauxCustom"+(nbr_niveau_default+1+o)+".txt");

				if (!ff.exists()) {
					ff.createNewFile();
				}

				FileReader ffw=new FileReader(ff.getAbsoluteFile());

				BufferedReader bf = new BufferedReader(ffw);

				for(int i = 0; i<16; i++){

					String s = bf.readLine();

					String[] st = s.split(" ");

					for(int j = 0; j<st.length; j++){
						Custom[i][j] = Integer.parseInt(st[j]);
					}
				}

				boolean custom_heros = false;
				boolean custom_exit = false;

				for(int i=0; i<Custom.length; i++){
					for(int j=0; j<Custom[i].length; j++){
						if(Custom[i][j] == 2){
							custom_heros = true;
						}
						if(Custom[i][j] == 5){
							custom_exit = true;
						}
					}
				}
				if(custom_heros && custom_exit){
					lvl.put((nbr_niveau_default+1+o), Custom);

				}
				
				bf.close();
				ffw.close();
			}

		} catch(IOException e) {
			System.out.println("bugNiv");
		}
	}

	public void listerRepertoire(File repertoire){ 
		String [] listefichiers; 
		listefichiers=repertoire.list(); 
		for(int i=0;i<listefichiers.length;i++){ 
			if(listefichiers[i].endsWith(".txt")){ 
				nbr_niveaux = nbr_niveaux+1;
			} 
		} 
	}
}

