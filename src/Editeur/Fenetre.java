package Editeur;

import javax.swing.JFrame;

public class Fenetre extends JFrame {
	
	public Fenetre(String s, int w, int h) {
		super(s);
		this.setSize(w, h);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		
		Editeur e = new Editeur();
		this.setContentPane(e);
		this.pack();
		this.setLocationRelativeTo(null);
	}
	
	public static void main(String[] args) {
		new Fenetre("Editeur", 401, 600);
	}
}
