package Editeur;
import MonJeu.Niveaux;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import MonJeu.Sprites;

public class Editeur extends JPanel implements MouseListener, MouseWheelListener, ActionListener{

	private Integer[][] Niveau_customisable; 

	private int Object_a_dessiner = 1;

	private boolean joueur;

	private boolean sortie;

	private JButton b;
	
	private Sprites sprites;
	
	public int nbr_niveaux = Niveaux.nbr_niveau_default;

	
	/**
	 * Constructeur
	 */
	public Editeur(){
		this.sprites = new Sprites();
		this.setPreferredSize(new Dimension(401, 600));

		Niveau_customisable = new Integer[16][16];
		for(int i = 0; i<Niveau_customisable.length; i++){
			for(int j = 0; j<Niveau_customisable[i].length; j++){
				Niveau_customisable[i][j] = 0;
				if(i == 0 || i == Niveau_customisable.length-1) Niveau_customisable[i][j] = 1;
				if(j == 0 || j == Niveau_customisable[i].length-1)  Niveau_customisable[i][j] = 1;
			}
		}

		this.joueur = false;
		this.sortie = false;

		this.b = new JButton("Sauvegarder");
		this.add(this.b);
		this.b.addActionListener(this);
		

		if(!Files.exists(Paths.get("niveaux"))){
			new File("niveaux").mkdir();
		}

		addMouseListener(this);
		addMouseWheelListener(this);
	}

	/**
	 * paintComponent, dessine les images de l'editeur
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		for(int i = 0; i<Niveau_customisable.length; i++){
			for(int j = 0; j<Niveau_customisable[i].length; j++){
				if(Niveau_customisable[i][j] == 0){
					g.drawImage(this.sprites.getImage("SOL"), (i * 25), (j * 25), 25, 25, null);
				}

				if(Niveau_customisable[i][j] == 1){
					g.drawImage(this.sprites.getImage("MUR"), (i * 25), (j * 25), 25, 25, null);
				}

				if(Niveau_customisable[i][j] == 2){
					g.drawImage(this.sprites.getImage("SOL"), (i * 25), (j * 25), 25, 25, null);
					g.drawImage(this.sprites.getImage("AVENTURIER"), (i * 25), (j * 25), 25, 25, null);
				}

				if(Niveau_customisable[i][j] == 3){
					g.drawImage(this.sprites.getImage("SOL"), (i * 25), (j * 25), 25, 25, null);
					g.drawImage(this.sprites.getImage("MONSTRE"), (i * 25), (j * 25), 25, 25, null);
				}

				if(Niveau_customisable[i][j] == 4){
					g.drawImage(this.sprites.getImage("SOL"), (i * 25), (j * 25), 25, 25, null);
					g.drawImage(this.sprites.getImage("PIEGE"), (i * 25), (j * 25), 25, 25, null);
				}

				if(Niveau_customisable[i][j] == 5){
					g.drawImage(this.sprites.getImage("SOL"), (i * 25), (j * 25), 25, 25, null);
					g.drawImage(this.sprites.getImage("SORTIE"), (i * 25), (j * 25), 25, 25, null);
				}

				if(Niveau_customisable[i][j] == 6){
					g.drawImage(this.sprites.getImage("SOL"), (i * 25), (j * 25), 25, 25, null);
					g.drawImage(this.sprites.getImage("POTION"), (i * 25), (j * 25), 25, 25, null);
				}

			}
		}

		int w = 16*25;
		int h = w;

		for(int i=0; i<17;i++){
			g.drawLine(0,(i*25),w,(i*25));
		}

		for(int i=0; i<17;i++){
			g.drawLine((i*(w/16)),0,(i*(w/16)), h);
		}

		g.drawString("-Clique droit=cr�er: 1=Mur, 2=Aventurier, 3=Monstre, 4=Trappe", 50, 450);
		g.drawString("-5=Sortie, 6=Potion", 50, 475);
		g.drawString("-Clique gauche=effacer", 50, 500);
		g.drawString("-Molette de la souris pour modifier l'objet � construire", 50, 525);
		g.drawString("-Objet � construire selectionn� : "+Integer.toString(this.Object_a_dessiner), 50, 550);
		g.drawString("-Attention ! Un seul aventurier et une seule sortie !", 50, 575);
	}

	/**
	 * Methode gerant les cliques de souris
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if(SwingUtilities.isLeftMouseButton(e)){
			int posx = e.getX();
			int posy = e.getY();
			int w = 16*25;
			for(int i = 0; i<Niveau_customisable.length; i++){
				for(int j = 0; j<Niveau_customisable[i].length; j++){
					if(posx <= (i+1)*25 && posx >= i*25){
						if(posy <= (j+1)*25 && posy >= j*25){
							if(i == 0 || j == 0 || i == Niveau_customisable.length || j == Niveau_customisable[i].length){
								this.repaint();
							}
							else{
								if(this.Object_a_dessiner == 2){
									if(!joueur){
										Niveau_customisable[i][j] = this.Object_a_dessiner;
										this.joueur = true;
									}
								}
								else if(this.Object_a_dessiner == 5){
									if(!sortie){
										Niveau_customisable[i][j] = this.Object_a_dessiner;
										this.sortie = true;
									}
								}
								else{
									Niveau_customisable[i][j] = this.Object_a_dessiner;
								}
								this.repaint();
							}
						}
					}
				}
			}	
		}

		if(SwingUtilities.isRightMouseButton(e)){
			int posx = e.getX();
			int posy = e.getY();
			int w = 16*25;
			for(int i = 0; i<Niveau_customisable.length; i++){
				for(int j = 0; j<Niveau_customisable[i].length; j++){
					if(posx <= (i+1)*25 && posx >= i*25){
						if(posy <= (j+1)*25 && posy >= j*25){

							if(i == 0 || j == 0 || i == Niveau_customisable.length-1 || j == Niveau_customisable[i].length-1){
								this.repaint();
							}
							else{
								if(Niveau_customisable[i][j] == 2){
									this.joueur = false;
								}
								if(Niveau_customisable[i][j] == 5){
									this.sortie = false;
								}
								Niveau_customisable[i][j] = 0;
								this.repaint();
							}
						}
					}
				}
			}
		}


	}

	@Override
	public void mousePressed(MouseEvent e) {


	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	/**
	 * Methode gerant la molette de souris et le choix de l'objet a construire
	 */
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {

		int wheel = e.getWheelRotation();
		System.out.println(wheel);
		if(wheel == -1 && this.Object_a_dessiner > 1){
			this.Object_a_dessiner--;
			this.repaint();
		}
		if(wheel == 1 && this.Object_a_dessiner < 6){
			this.Object_a_dessiner++;
			this.repaint();
		}

	}

	/**
	 * methode gerant le boutton sauvegarder
	 */
	public void actionPerformed(ActionEvent e){
		Object source = e.getSource();

		if(source == this.b){
			this.sauvegarder();
		}
	}

	/**
	 * Methode retournant le tableau d'entier du niveau customise
	 * @return niveau customise
	 */
	public Integer[][] getNiveauCustom(){
		return this.Niveau_customisable;
	}


	public void listerRepertoire(File repertoire){ 
		String [] listefichiers; 
		listefichiers=repertoire.list(); 
		for(int i=0;i<listefichiers.length;i++){ 
			if(listefichiers[i].endsWith(".txt")){ 
				nbr_niveaux = nbr_niveaux+1;
			} 
		} 
	}

	/**
	 * methode sauvegardant le fichier dans un .txt
	 */
	public void sauvegarder(){
		try{
			File repertoire = new File("niveaux");
			this.listerRepertoire(repertoire);
			
			File ff=new File("niveaux/NiveauxCustom"+(nbr_niveaux+1)+".txt");
			
			if (!ff.exists()) {
				ff.createNewFile();
			}

			FileWriter ffw=new FileWriter(ff.getAbsoluteFile());

			BufferedWriter bf = new BufferedWriter(ffw);
			
			for(int i = 0; i<Niveau_customisable.length; i++){
				for(int j = 0; j<Niveau_customisable[i].length; j++){
					bf.write(this.Niveau_customisable[i][j]+" ");

					if(j == Niveau_customisable[i].length-1){
						bf.newLine();
					}
				}
			}
			
			bf.write("\n");

			bf.close();
			ffw.close(); 

		} catch (Exception e) {
			System.out.println("bug");
		}
		
	}
	
}
